var app = {
  loadEditor: function() {
    $('.editable').summernote({toolbar: [
        ['font', ['bold', 'italic', 'underline', 'clear']],
        ['fontname', ['style', 'fontname']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']],
        ['table', ['table']],
        ['insert', ['link', 'picture']],
        ['view', ['fullscreen', 'codeview']],
    ]});
  },

  showModalAlert: function(title, message, type) {
    type = type || 'info';

    var modal = $('<div id="secondModal" data-alert class="alert-box ' + type + ' radius reveal-modal" data-reveal><a class="close-reveal-modal">&#215;</a></div>');

    if (title) {
      var h3 = $('<h3>');
      h3.text(title);
      modal.append(h3);
    }

    if (message) {
      var div = $('<div>');
      div.text(message);
      modal.append(div);
    }

    // set buttons
    var buttonOk = $('<a href="#" class="button secondary tiny btn-ok radius">Ok</a>');
    buttonOk.click(function(){
      modal.foundation('reveal', 'close');
    });
    modal.append(buttonOk);

    modal.foundation('reveal', 'open');
    return modal;
  },
  getNewPage: function() {
    $.get('/new/').done(function(data){
      $(data).foundation('reveal', 'open');
      app.loadEditor();
    });
  },
  getEditPage: function(pageId) {
    $.get('/edit/'+pageId).done(function(data){
      $(data).foundation('reveal', 'open');
      app.loadEditor();
    });
  },
  savePage: function(elem, pageId) {
    var form = $(elem);
    pageId = pageId || '';
    
    // set back code to textarea
    $('.editable').html($('.editable').code());

    $.ajax({
      url: '/page/'+pageId,
      data: form.serializeArray(),
      type: 'PUT'
    }).done(function(data) {
      if (data.status === 'success'){
        app.showModalAlert('', 'Changed saved', 'success').bind('closed', function(){
          location.reload();
        });
      } else if (data.status === 'error'){
        app.showModalAlert('', 'Could not save page', 'alert');
      }
    }).fail(function(){
      app.showModalAlert('', 'Could not save page', 'alert');
    });

    return false;
  },
  removePage: function(pageId) {
    pageId = pageId || '';
    $.ajax({
      url: '/page/'+pageId,
      type: 'DELETE'
    }).done(function(){
      app.showModalAlert('', 'Page deleted', 'success').bind('closed', function(){
        location.reload();
      });
    }).fail(function(){
      app.showModalAlert('', 'Could not delete page', 'success').bind('closed', function(){
        location.reload();
      });
    });
  }
};

$(document).foundation({
  reveal : {
    multiple_opened: true
  }
});
