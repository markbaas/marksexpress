var gulp = require('gulp'),
    //server = require('gulp-express'),
    sass = require('gulp-sass'),
    supervisor = require('gulp-supervisor'),
    shell = require('gulp-shell'),
    env = require('gulp-env'),
    casperJs = require('gulp-casperjs');

gulp.task('supervisor', function() {
  supervisor('app/server.js', {
    watch: __dirname + '/app',
    extensions: ['js','html','css']
  });
});

gulp.task('styles', function() {
  return gulp.src('public/stylesheets/*.scss')
    .pipe(sass({ style: 'expanded', includePaths: [__dirname + '/public/components/foundation/scss']}))
    .pipe(gulp.dest('public/stylesheets'));
});

gulp.task('watch', function() {
  gulp.watch('public/stylesheets/app.scss', ['styles']);
});

gulp.task('fixtures', shell.task(['node tests/loaddata.js']));

gulp.task('protractor', function() {
  env({ vars: { 
    'NODE_ENV': 'test'
  }});
  gulp.src('tests/test_*.js')
    .pipe(casperJs()); //run casperjs test
});

gulp.task('test', ['protractor'], function() { });

gulp.task('default', ['styles', 'watch', 'supervisor'], function() { });