var sequelize_fixtures = require('sequelize-fixtures'),
    models = require('../app/models'),
    fs = require('fs');

// DROP all
models.User.drop();
models.Page.drop();
models.Template.drop();

// create tables again
models.sequelize.sync().success(function () {
  // add default user
  models.User.findAll({where: {'username': 'admin'}}).success(function(result){
    if (!result[0]) {
      models.User.register('admin', 'admin', function() {});
    }
  });
  
  // now populate tables
  var templates = [];
  fs
    .readdirSync(__dirname + '/fixtures')
    .filter(function(file) {
      return (file.indexOf('.') !== 0);
    })
    .forEach(function(f){
      templates.push(__dirname + '/fixtures/' + f);
  });
  sequelize_fixtures.loadFiles(templates, models);
  
});