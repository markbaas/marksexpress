// ==============
// Define required modules
// ==============

var models = require('./models'),
    extend = require('extend'),
    passport = require('passport'),
    Promise = require('bluebird'),
    config = require('./config'),
    sendgrid = require('sendgrid')(config.sendgrid.username, config.sendgrid.api_key);

// ===============
// Common methods
// ===============
var common = {
  getAllPages: function() {
    return models.Page.findAll({ 
      where: {'Page.skipMenu': false, 'Page.parentId': null},
      order: 'Page.weight ASC',
      include: [ { model: models.Page, as: 'Children' }  ] // Todo results in ugly query, needs to patch sequelize
    });
  },
  verifyUser: function(user) {
    return new Promise(function(resolve, reject){
      if (user) {
        resolve(true);
      } else {
        reject(false);
      }
    });
  },
  renderPageNotFound: function(res) {
    res.render('error', {error: {status: 404, message: 'Could not find page'}, statusCode: 404} );
  },
  getCurrentPage: function(pageId) {
    // get current page
    return new Promise(function(resolve, reject) {
      models.Page.find({
        where: {id: pageId},
        include: [ { model: models.Page, as: 'Children' }, models.Template ] // Todo results in ugly query, needs to patch sequelize
      }).success(function(page){
        resolve(page);
      }).fail(function(){
        reject();
      });
    });
  },
  savePage: function(pageId, form) {
    var page;
    
    var savePromise = function(page) {
      return new Promise(function(resolve) {
        page.content = form.content;
        page.title = form.title;
        page.shortName = form.shortName;
        page.weight = form.weight;
        page.skipMenu = form.skipMenu;
        page.templateId = form.templateId;
        page.parentId = form.parentId || null;

        page.save().success(function(){
          resolve({response: { status: 'success' }, status: 200});
        }).error(function(){
          resolve({response: { status: 'error', error: 'Could not save page.' }, status: 500});
        });
      });
    };
    
    return new Promise(function(resolve){
      if (!form.isValid) {
        resolve({ status: 'error', errors: form.errors }, 500);
      } else {
        if (pageId) {
          // existing -> update
          common.getCurrentPage(pageId).then(function(page){
            savePromise(page).then(function(data){
              resolve(data);
            });
          }).catch(function(){
            resolve({response: { status: 'error', error: 'Database error' }, status: 500});
          });
        } else {
          // new
          page = models.Page.build();
          savePromise(page).then(function(data){
            resolve(data);
          });
        }
      }    
    });
  },
  deletePage: function(pageId) {
    return new Promise(function(resolve) {
      common.getCurrentPage(pageId).then(function(page){
        page.destroy().success(function() {
          resolve({response: { 'status': 'success' }, status: 200});
        }).error(function(){
          resolve({response: { 'status': 'error', error: 'Could not delete page' }, status: 500});
        });
      }).error(function(){
        resolve({response: { 'status': 'error', error: 'Could not find page'}, status: 500});
      });
      
    });
  }
};

// ===============
// Controllers
// ===============
var controllers = {
  
  // ==================
  // SavePage Controller
  // ==================
  SavePage: function(req, res) {    
    common.verifyUser(req.user).then(function(){
      common.savePage(req.params.id, req.form).then(function(data) {
        res.type('json').status(data.status).send(JSON.stringify(data.response));
      });
    }).catch(function(){
      res.status(403).send('Not logged in');
    });
  },
  
  // ======================
  // DeletePage Controller
  // ======================
  DeletePage: function(req, res) {
    common.deletePage(req.params.id).done(function(data){
      res.type('json').status(data.status).send(JSON.stringify(data.response));
    });
  },
  
  // ==================
  // LoadPage Controller
  //
  // This is the base page controller which other page controllers are based on.
  // ==================
  LoadPage: function(req, res, extra_vars) {
    var pageId = (req.params.id) ? req.params.id : 1;
    var pages = [];
    
    // get all pages for menu
    common.getAllPages().success(function(result){
      pages = result;
      return common.getCurrentPage(pageId);
    }).then(function(page){
      // check if page valid
      if (!page) {
        common.renderPageNotFound(res);
        return;
      }
      
      // get template
      var template = (page.Template) ? page.Template.shortName : 'index';
      
      // set vars
      var vars = { title: page.title, pages: pages, page: page, user: req.user, csrf_token: req.csrfToken()};
      
      // extend with extra vars
      if (extra_vars) {
        extend(vars, extra_vars);
      }

      // render page
      res.render(template, vars);
    }); 
  },
  
  // ==================
  // LoadCategoryPage Controller
  // ==================
  LoadCategoryPage: function(req, res) {
    // define vars
    var vars = {};

    // set page id
    models.Page.findAll({ 
      where: {'shortName': req.params.category}, 
      limit: 1 
    }).success(function(result){
      if (result[0]) {
        req.params.id = result[0].id;     
        
        // get & render page
        controllers.LoadPage(req, res, vars);
      } else {
        common.renderPageNotFound(res);
      }
    });
  },
  
  // ==================
  // LoadSubcategoryPage Controller
  // ==================
  LoadSubcategoryPage: function(req, res) {
    // define vars
    var vars = {};

    // set page id
    models.Page.findAll({ 
      where: {'shortName': req.params.subcategory}, 
      limit: 1 
    }).success(function(result){
      if (result[0]) {
        req.params.id = result[0].id;     
      }
      // get products
      return models.Page.findAll({
        where: {'Page.type': 'product'}
      });
    }).success(function(products){
      vars.products = products;
      
      // now call loadpage controller with params
      controllers.LoadPage(req, res, vars);
    });
  },
  
  // ==================
  // LoadLoginPage Controller
  // ==================
  LoadLoginPage: function(req, res) {
    var pages = [];
    
    // get all pages for menu
    common.getAllPages().success(function(result){
      pages = result;
      res.render('login', { pages: pages, title: 'Login', failed: (req.query.failed !== undefined) ? req.query.failed : false, csrf_token: req.csrfToken()} );
    });
  },
  
  // ================
  // Login Controller
  // ================
  Login: passport.authenticate('local', { successRedirect: '/', failureRedirect: '/login?failed=true' }),
  
  // ===================
  // New Page Controller
  // -------------------
  // Renders the new page dialog
  // ===================
  NewPage: function(req, res) {
    var templates = [],
        parents = [];
    
    if (common.verifyUser(req, res)) {
      // get templates
      models.Template.findAll().success(function(data){
        templates = data;
      // get parents
      }).then(models.Page.getPotentialParents).then(function(data) {
        parents = data;
        res.render('admin/newpage.html', { templates: templates, parents: parents, csrf_token: req.csrfToken() } );
      });
    }
  },
  
  // ==================
  // EditPage Controller
  // -------------------
  // Render the edit page dialog
  // ==================
  EditPage: function(req, res) {
    var templates = [],
        parents = [];
    var pageId = req.params.id;
    
    if (common.verifyUser(req, res)) {
      // get templates
      models.Template.findAll().success(function(data){
        templates = data;
      // get parents
      }).then(models.Page.getPotentialParents).then(function(data) {
        parents = data;
        return common.getCurrentPage(pageId);
      }).success(function(page) {
        res.render('admin/newpage.html', { templates: templates, parents: parents, page: page, csrf_token: req.csrfToken() } );
      });
    }
  },
  
  // ======================
  // Send mail Controller
  // ----------------------
  // 
  // ======================
  SendMail: function(req, res) {
    sendgrid.send({
      to:       config.email,
      from:     req.form.email,
      fromName: req.form.name,
      subject:  req.form.subject,
      text:     req.form.message
    }, function(err, json) {
      
      var status;
      
      if (err) { 
        console.log(err);
        status = 500;
      } else {
        status = 200;
      }
      
      res.type('json').status(status).send(JSON.stringify(json));
    });
    
    
  },
  
  
  
};

module.exports = controllers;
