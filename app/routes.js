var controllers = require('./controllers'),
    forms = require('./forms'),
    express = require('express'),
    router = express.Router();

// ===============
// Routes
// ===============
var routes = [
  { 
    pattern: '/',             
    methods: ['get'],
    controller: controllers.LoadPage 
  },
  {
    pattern: '/contact/?',
    methods: ['post'],
    controller: controllers.SendMail,
    middleware: forms.Contact
  },
  { 
    pattern: '/login/?',
    methods: ['get'],
    controller: controllers.LoadLoginPage
  },
  {
    pattern: '/login/?',
    methods: ['post'],
    controller: controllers.Login
  },
  {
    pattern: '/edit/:id/?',
    methods: ['get'],
    controller: controllers.EditPage,
    middleware: forms.editPage
  },
  {
    pattern: '/page/:id/?',
    methods: ['put'],
    controller: controllers.SavePage,
    middleware: forms.Page
  },
  {
    pattern: '/page/:id/?',
    methods: ['delete'],
    controller: controllers.DeletePage,
  },
  {
    pattern: '/new/?',
    methods: ['get'],
    controller: controllers.NewPage
  },
  
  // Dynamic patterns last
  { 
    pattern: '/page/:id/?',
    methods: ['get'],
    controller: controllers.LoadPage 
  },
  { 
    pattern: '/:category/?',
    methods: ['get'],
    controller: controllers.LoadCategoryPage 
  },
  { 
    pattern: '/:category/:subcategory/?',
    methods: ['get'],
    controller: controllers.LoadSubcategoryPage
  },
  {
    pattern: '/page/?',
    methods: ['put'],
    controller: controllers.SavePage,
    middleware: forms.Page
  }
];

// Configure routes
for (var i in routes) {
  var route = routes[i];
  for (var j in route.methods) {
    if (route.middleware) {
      router[route.methods[j]](route.pattern, route.middleware, route.controller);
    } else {
      router[route.methods[j]](route.pattern, route.controller);
    }
  }
}

module.exports = router;