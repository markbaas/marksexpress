var form = require('express-form');
var field = form.field;

var forms = {
  Page: form(
    field('title').trim().required(),
    field('shortName').trim().required(),
    field('content').trim().required(),
    field('weight').isInt('Weight must be a number'),
    field('skipMenu').toBoolean(),
    field('templateId').required(),
    field('parentId').isInt('ParentId should be a number')
  ),
  
  Contact: form(
    field('name').trim().required(),
    field('email').trim().required().isEmail(),
    field('phone').trim().is(/[\d\.\- ]+/),
    field('subject').trim().required(),
    field('message').required()
  )
    
};

module.exports = forms;