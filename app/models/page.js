
module.exports = function(sequelize, DataTypes) {
  var Page = sequelize.define("Page", {
    title: DataTypes.STRING,
    shortName: DataTypes.STRING,
    content: DataTypes.STRING,
    weight: DataTypes.INTEGER,
    skipMenu: DataTypes.BOOLEAN,
    type: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        Page.hasMany(Page, {as: 'Children', foreignKey: 'parentId'});
        Page.belongsTo(models.Template, {foreignKey: 'templateId'});
      },
      
      getPotentialParents: function() {

        return Page.findAll().success(function(data) {
          var parents = [],
              rows = {};
          
          // map the rows with parentId
          for (var i in data) {
            var row = data[i];
            rows[row.id] = row.parentId;
          }
          
          // level getter
          var getLevel = function(id, n) {
            if (rows[id] !== null) {
              n = getLevel(rows[id], n);
            }
            n++;
            return n;
          };
          
          // get level
          for (var id in rows) {
            var level = getLevel(id, 0);
            // Level can only go 4 deep i.e. index, products, category, subcategory
            if (level <= 4) {
              parents.push(data[id]);
            }
          }
          
          return parents;
        });
      }
    },
    instanceMethods: {
    }
  });
  
  return Page;
};