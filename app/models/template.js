"use strict";

module.exports = function(sequelize, DataTypes) {
  var Template = sequelize.define("Template", {
    title: DataTypes.STRING,
    shortName: DataTypes.STRING,
  }, {
    classMethods: {
      associate: function(models) {
      }
    },
    instanceMethods: {
    }
  });
  
  return Template;
};