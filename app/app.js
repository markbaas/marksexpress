var express = require('express'),
    path = require('path'),
    favicon = require('serve-favicon'),
    logger = require('morgan'),
    cookieParser = require('cookie-parser'),
    bodyParser = require('body-parser'),
    swig = require('swig'),
    filters = require('./filters'),
    passport = require('passport'),
    session = require('express-session'),
    csrf = require('csurf');

var routes = require('./routes');

var app = express();

// view engine setup
app.engine('html', swig.renderFile);
app.set('views', path.join(__dirname, 'templates'));
app.set('view engine', 'html');
filters.setFilters(swig);

// uncomment after placing your favicon in /public
app.use(favicon(__dirname + '/../public/images/logo.png'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(session({ secret: 'super-secret', resave: true, saveUninitialized: true }));
app.use(csrf());
app.use(passport.initialize());
app.use(passport.session());

// rewrite public to /
app.use(function(req, res, next) {
  
  if (req.url.match(/^\/public/)) {
    console.log(req.url, req.url.replace(/^\/public/, ''));
    req.url = req.url.replace(/^\/public/, '');
  }
  next();
});
app.use(express.static(path.join(__dirname, '../public')));

// load routes
app.use('/', routes);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


// passport configuration
var models = require('./models');
passport.use(models.User.createStrategy());
passport.serializeUser(models.User.serializeUser());
passport.deserializeUser(models.User.deserializeUser());
module.exports = app;
