var substr = function(input, length) {
  return input.substr(0, length) + "...";
};

module.exports.setFilters = function(swig) {
  swig.setFilter('substr', substr);
};